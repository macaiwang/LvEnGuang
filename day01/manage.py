from flask import session
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager

from day01.info import create_app, db

# 创建 app，并传入配置模式：development / production
app = create_app('development')
# 使用flask_script管理app
manager = Manager(app)
# 关联app和db
Migrate(app, db)
# 添加迁移命令
manager.add_command('db', MigrateCommand)


@app.route('/')
def index():
    session['name'] = 'xiaoming'
    session['age'] = '18'
    print(session.get('name'))
    print(session.get('age'))
    return 'index333'


if __name__ == '__main__':
    app.run(BEBUG=True)
